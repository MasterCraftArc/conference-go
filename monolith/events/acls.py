import requests
import json



def get_photo(city):
    headers = {"Authorization":"h7SAoyXvYsJ3duDPEH7x3hN1woXNgC0LnYecJVJH1RbMbs3ELwau6TiJ"}
    params = {
        "per_page":1,
        "query":city,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except(KeyError):
        return{"picture_url": None}


def get_weather(city):
    api_key = "9d71c0c67a0ee0aae96f9583303666a4"
    url = f"https://api.openweathermap.org/data/2.5/weather?q={city}&appid={api_key}&units=imperial"
    response = requests.get(url)
    content = json.loads(response.text)
    try:
        weather = content["main"]["temp"]
        return {"weather": weather}
    except KeyError:
        return {"weather": None}
