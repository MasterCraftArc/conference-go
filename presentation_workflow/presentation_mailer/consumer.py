import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

parameters = pika.ConnectionParameters(host='rabbitmq')
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue='presentation_approval')
channel.queue_declare(queue='presentation_rejection')

def process_approval(ch, method, properties, body):
    print("  Received %r" % body)
    content = json.loads(body)
    send_mail(
        'Presentation Accepted!',
        f'{content["presenter_name"]} I am happy to tell you that your presentation has been accepted at {content["title"]}',
        'admin@conference.go',
        [f'{content["presenter_email"]}'],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    print("  Received %r" % body)
    content = json.loads(body)
    send_mail(
        'Presentation rejected',
        f'{content["presenter_name"]} I am happy to tell you that your presentation has been rejected at {content["presenter_email"]}',
        'admin@conference.go',
        [f'{content["presenter_email"]}'],
        fail_silently=False,
    )


channel.basic_consume(
    queue='presentation_approval',
    on_message_callback=process_approval,
    auto_ack=True,
    )
channel.basic_consume(
    queue='presentation_rejection',
    on_message_callback=process_rejection,
    auto_ack=True,
)
channel.start_consuming()
